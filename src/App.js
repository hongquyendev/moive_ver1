import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import SpinnerComponent from "./Components/SpinnerComponent/SpinnerComponent";
import Layout from "./HOC/Layout";
import DetailPage from "./Page/DetailPage/DetailPage";
import HomePage from "./Page/HomePage/HomePage";
import LoginPage from "./Page/LoginPage/LoginPage";

function App() {
  return (
    <div>
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
