import moment from "moment";
import React from "react";

export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex space-x-10 p-5">
      <img className="w-32 h-48 object-cover" src={phim.hinhAnh} alt="" />
      <div>
        <p className="font-medium text-xl text-gray-800 mb-5">
          {phim.tenPhim}
          <div className="grid grid-cols-2 gap-4">
            {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu) => {
              return (
                <div className="bg-slate-100 text-green-600 p-2 rounded">
                  {moment(lichChieu.ngayChieuGioChieu).format(
                    "DD-MM-YYYY ~ hh:mm"
                  )}
                </div>
              );
            })}
          </div>
        </p>
      </div>
    </div>
  );
}
