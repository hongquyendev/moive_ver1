import React, { Component } from "react";
import ListMove from "./ListMove";
import TabMovie from "./TabMovie";

export default class HomePage extends Component {
  render() {
    return (
      <div className="space-y-20">
        <ListMove />
        <div className="justify-center flex">
          <TabMovie />
        </div>
      </div>
    );
  }
}
