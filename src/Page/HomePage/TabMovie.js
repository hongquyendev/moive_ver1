import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import SpinnerComponent from "../../Components/SpinnerComponent/SpinnerComponent";
import { movieService } from "../../Service/movieService";
import ItemTabMovie from "./ItemTabMovie";

export default function TabMovie() {
  const [dataMovie, setDataMovie] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    movieService
      .getMovieByTheater()
      .then((res) => {
        setIsLoading(false);
        setDataMovie(res.data.content);
        console.log("res: ", res);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log("err: ", err);
      });
  }, []);

  let renderContent = () => {
    return dataMovie.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-16" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <Tabs.TabPane
                  tab={renderTenCumRap(cumRap)}
                  key={cumRap.maCumRap}
                >
                  <div
                    style={{ height: 700, overflowY: "scroll" }}
                    className="shadow"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie phim={phim} />;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  const renderTenCumRap = (cumRap) => {
    return (
      <div className="text-left w-60">
        <p className="text-green-700 truncate">{cumRap.tenCumRap}</p>
        <p className="text-black truncate">{cumRap.diaChi}</p>
        <button className="text-red-400">[chi tiết]</button>
      </div>
    );
  };

  return (
    <div className="container mx-auto py-20 pb-96">
      {/* {isLoading ? <SpinnerComponent /> : ""} */}
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
