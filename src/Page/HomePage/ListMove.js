import { Card } from "antd";
import Meta from "antd/lib/card/Meta";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getMovieListActionServ } from "../../Redux/Actions/movieAction";

export default function ListMove() {
  let dispatch = useDispatch();
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });
  console.log("movieList: ", movieList);
  useEffect(() => {
    dispatch(getMovieListActionServ());
  }, []);

  let renderMovieList = () => {
    return movieList
      .filter((item) => {
        return item.hinhAnh !== null;
      })
      .map((item) => {
        let { hinhAnh, tenPhim, biDanh, trailer, maPhim } = item;
        return (
          <NavLink to={`detail/${maPhim}`}>
            <Card
              hoverable
              style={{
                width: "100%",
              }}
              className="shadow-xl hover:shadow-slate-600 hover:shadow-xl"
              cover={<img alt="example" src={hinhAnh} />}
            >
              <Meta
                title={<p className="text-blue-500">{tenPhim}</p>}
                description={trailer}
              />

              <button className="w-full bg-red-500 text-white rounded-sm mt-5 text-xl font-medium py-2">
                Mua vé
              </button>
            </Card>
          </NavLink>
        );
      });
  };

  return (
    <div className="container mx-auto grid grid-cols-4 gap-10">
      {renderMovieList()}
    </div>
  );
}
