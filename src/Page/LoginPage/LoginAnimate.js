import React from "react";
import Lottie from "lottie-react";
import bgAnimate from "../../Assets/loginAnimate.json";
export default function LoginAnimate() {
  return (
    <div className="transform -translate-y-24">
      <Lottie animationData={bgAnimate} height={200} />
    </div>
  );
}
