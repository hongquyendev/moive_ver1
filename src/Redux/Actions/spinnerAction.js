import { BAT_LOADING, TAT_LOADING } from "../Constants/spinnerContants";

export const batLoandingAction = () => {
  return {
    type: BAT_LOADING,
  };
};
export const tatLoandingAction = () => {
  return {
    type: TAT_LOADING,
  };
};
