import { movieService } from "../../Service/movieService";
import { SET_MOVIE_LIST } from "../Constants/movieConstans";

export let getMovieListActionServ = () => {
  return (dispatch) => {
    movieService
      .getMovieList()
      .then((res) => {
        dispatch({
          type: SET_MOVIE_LIST,
          payload: res.data.content,
        });
        console.log("res: ", res);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };
};
