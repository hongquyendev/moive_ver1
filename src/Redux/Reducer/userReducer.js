import { localStorageServ } from "../../Service/localStorageService";

let initialState = {
  userInfor: localStorageServ.user.get(),
};

export let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN": {
      state.userInfor = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
