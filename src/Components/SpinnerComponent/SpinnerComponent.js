import React from "react";
import { useSelector } from "react-redux";
import { RingLoader } from "react-spinners";

export default function SpinnerComponent() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer;
  });
  return isLoading ? (
    <div
      className="h-screen w-screen fixed top-0 left-0 overflow-hidden flex justify-center items-center z-50"
      style={{
        backgroundColor: "#FFEBAD",
      }}
    >
      <RingLoader color="#FF9494" size={150} />
    </div>
  ) : (
    ""
  );
}
