import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../Redux/Actions/userAction";
import { localStorageServ } from "../../Service/localStorageService";

export default function UserNav() {
  let dispatch = useDispatch();
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  const renderNav = () => {
    if (userInfor) {
      return (
        <div className="flex space-x-5 items-center">
          <p>{userInfor.hoTen}</p>
          <button
            onClick={handleLogout}
            className="border-red-500 text-red-500 rounded px-5 py-3 border-2"
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="flex space-x-5 items-center">
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-blue-500 text-blue-500 rounded px-5 py-3 border-2"
          >
            Đăng nhập
          </button>
          <button className="border-orange-500 text-orange-500 rounded px-5 py-3 border-2">
            Đăng ký
          </button>
        </div>
      );
    }
  };

  const handleLogout = () => {
    localStorageServ.user.remove();
    dispatch(loginAction(null));
  };

  return <div>{renderNav()}</div>;
}
