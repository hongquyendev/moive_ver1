import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function HeaderTheme() {
  return (
    <div className="h-20 px-10 flex items-center justify-between shadow-lg">
      <NavLink to={"/"}>
        <div style={{ color: "#023047" }} className="logo text-2xl font-medium">
          LOGO
        </div>
      </NavLink>
      <UserNav />
    </div>
  );
}
